import React, {useState, useEffect} from 'react';
import './App.css';
import Navbar from "./components/Nav";
import Products from "./components/Products";
import Cart from "./components/Cart";
import data from "./data";

function App() {
    const [products, setProduct] = useState([...data]);
    const [cartItems, setCartItem] = useState([]);
    const [keyWord, setKeyWord] = useState("");

    useEffect(() => {
        const results = data.filter(product => (product.title.includes(keyWord) || product.brand.includes(keyWord)));
        setProduct(results)
    }, [keyWord]);

    const addCartItem = (id) => {
        const result = products.find(item => id === item.id);

        setCartItem((cartItems) => {

            const items = cartItems.findIndex((item) => item.id === id);
            if (items === -1) {
                return [...cartItems, {...result, quantity: 1}]
            } else {
                return cartItems.map(item => item.id === id ? {...item, quantity: parseInt(item.quantity) + 1} : item)
            }

        });


    };

    const removeItem = (id) => {
        setCartItem((items) => {
            return items.filter((item) => item.id !== id)
        })
    };

    const clearCart = () => {
        const result = window.confirm("Are You Sure Want to Clear Your Cart");
        if (result === true){
            setCartItem([]);
        }
    };


    return (
        <div className="App">
            <Navbar setKeyWord={setKeyWord}/>
            <Products products={products} addCartItem={addCartItem}/>
            <Cart cartItems={cartItems} removeItem={removeItem} clearCart={clearCart}/>
        </div>
    );
}

export default App;
