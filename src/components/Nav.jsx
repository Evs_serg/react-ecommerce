import React from "react";

const Navbar = ({setKeyWord}) => {

    const handleSearch = (e) => {
        setKeyWord(e.target.value)
    };

    return (
        <div className='NavBar'>
            <span>My App</span>
            <input type='search' placeholder="Enter Your Keyword" onChange={handleSearch}/>
        </div>
    )
};

export default Navbar