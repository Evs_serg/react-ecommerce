import React, {useState} from "react";

const CartItem = ({id, title, price, quantity, removeItem}) => {
    return (
        <div className='cart-item'>
            <button onClick={() => removeItem(id)}>X</button>
            <div className='info'>
                <span>{title} x{quantity}</span>
                <span>{price}</span>
            </div>
        </div>
    )
};

const Cart = ({cartItems, removeItem, clearCart}) => {
    const [checkout, setCheckout] = useState(false);
    const [address, setAddress] = useState("");

    const total = cartItems.reduce((sum, current) => sum + (current.quantity * current.price), 0);

    const handleCheckout = () => {
        setCheckout(prev => !prev)
    };

    const handleAddress = (e) => {
        setAddress(e.target.value)
    };

    return (
        <div className='cart'>
            <h4>Cart Items</h4>
            <div className='cart-items'>
                {cartItems.length === 0 && (
                    <div className='cart-item'>
                        <div className='info'>
                            <span>Your Cart is Empty</span>
                        </div>
                    </div>
                )}
                {
                    cartItems.map(item => <CartItem key={item.id} {...item} price={item.price * item.quantity} removeItem={removeItem}/>)
                }
                {
                    cartItems.length !== 0 && (
                        <>
                            <div className='cart-item'>
                                <div className='info'>
                                    <span>Total</span>
                                    <span>$ {total}</span>
                                </div>
                            </div>
                            <div className='cart-item'>
                                <div className='info'>
                                    <button onClick={clearCart}>Clear</button>
                                    <button onClick={handleCheckout}>{checkout === false ? "Checkout" : "Hide"}</button>
                                </div>
                            </div>
                        </>
                    )
                }
                {
                    cartItems.length !== 0 && checkout === true && (
                        <div className='cart-item'>
                            <div className='info'>
                                <input type='text' placeholder='Enter Your Address' onChange={handleAddress}/>
                                <button onClick={clearCart} disabled={!address}>Checkout</button>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    )
};

export default Cart