import React from "react";

const Product = ({image_url, id, title, brand, price, addCartItem}) => {
    return (
        <div className='product'>
            <img src={image_url} alt={title}/>
            <div className='title'>
                <span>{title.length > 10 ? title.substr(0, 10) + "..." : title}</span>
                <span>{brand}</span>
            </div>
            <div className='actions'>
                <span>$ {price}</span>
                <button onClick={() => addCartItem(id)}>Add to cart</button>
            </div>
        </div>
    )
};

const Products = ({products, addCartItem}) => {
    return (
        <div className='products'>
            {
                products.map((product) => {
                    return <Product {...product} key={product.id} addCartItem={addCartItem}/>
                })
            }
        </div>
    )
};

export default Products